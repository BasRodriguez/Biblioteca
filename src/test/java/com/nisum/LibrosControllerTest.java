package com.nisum;

//import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.nisum.controller.LibroController;
import com.nisum.model.*;
import com.nisum.service.*;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class LibrosControllerTest {
	
	@Mock
	private LibrosService libroService;
	
	@InjectMocks
	private LibroController libroController;

	

	@Test
	public void crearLibro() {
		RestAssuredMockMvc.standaloneSetup(libroController);
		Libro libro = new Libro();
		libro.setAutor("Tolkien");
		libro.setId(1L);
		libro.setTitulo("El hobbit");
		
		Mockito.when(libroService.crearLibro(Matchers.any(Libro.class))).thenReturn(libro);
		
		given().
			contentType(ContentType.JSON).
			body(libro).
		when().
			post("/libro/crearLibro").
		then().
			body("titulo", equalTo(libro.getTitulo())).
			statusCode(201);
	}
	
	@Test
	public void obtenerLibroId(){
		RestAssuredMockMvc.standaloneSetup(libroController);
		Libro libro = new Libro();
		libro.setAutor("Tolkien");
		libro.setId(1L);
		libro.setTitulo("El hobbit");
		
		Mockito.when(libroService.obtenerLibro(anyLong())).thenReturn(libro);
		
		given().
			contentType(ContentType.JSON).
		when().
			get("/libro/obtenerLibro/{id}", 1).
		then().
			body("id", is(1)).
			statusCode(200);
	}
	
	@Test
	public void obtenerLibros(){
	RestAssuredMockMvc.standaloneSetup(libroController);
		List<Libro> libros = new ArrayList<Libro>();
		
		Mockito.when(libroService.obtenerTodos()).thenReturn((ArrayList<Libro>) libros);
		
		given().
			contentType(ContentType.JSON).
		when().
			get("/libro/obtenerTodos").
		then().
			statusCode(200);
	}
	
	@Test
	public void obtenerLibroTitulo(){
		RestAssuredMockMvc.standaloneSetup(libroController);
		Libro libro = new Libro();
		libro.setAutor("Tolkien");
		libro.setId(1L);
		libro.setTitulo("El hobbit");
		
		Mockito.when(libroService.obtenerLibro(anyString())).thenReturn(libro);
		
		given().
			contentType(ContentType.JSON).
		when().
			get("/libro/obtenerLibroPorTitulo/{titulo}", "El hobbit").
		then().
			body("titulo", is(libro.getTitulo())).
			statusCode(200);
	}
}

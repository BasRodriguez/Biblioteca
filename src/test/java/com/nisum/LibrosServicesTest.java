package com.nisum;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.runners.MockitoJUnitRunner;

import com.nisum.dao.LibrosDao;
import com.nisum.model.Libro;
import com.nisum.service.LibrosService;



@RunWith(MockitoJUnitRunner.class)
public class LibrosServicesTest {
	
	@Mock
	private LibrosDao librosDao;
	
	@InjectMocks
	private LibrosService librosService;
	
//	private Libro libro;
	
/*	@Before
	public void SetUp(){
		libro = new Libro();
		libro.setId(1L);
	}
*/	
	
	@Test
	public void deboCrearUnLibroYRetornarlo(){
		//arrange
		Libro libro = new Libro();
		
		
		//act
		when(librosDao.save(libro)).thenReturn(libro);
		Libro libroCreado = librosService.crearLibro(libro);
		
		//assert
		Assert.assertNotNull(libroCreado);
		Assert.assertEquals(libro, libroCreado);
	}
	
	@Test
	public void obtenerLibroPorId(){
		//arrange
		Libro libro = new Libro();
		libro.setId(1L);
		
		//libro.setTitulo("java");
		
		Libro libroRetornado = new Libro();
		
		//act
		when(librosDao.findOne(anyLong())).thenReturn(libro);
		libroRetornado = librosService.obtenerLibro(libro.getId());
				
		//assert
		Assert.assertNotNull(libroRetornado);		
	}
	
	@Test
	public void obtenerTodosLosLibros(){
		//arrange
		List<Libro> misLibros = new ArrayList<Libro>();
		//act
		when(librosDao.findAll()).thenReturn(misLibros);
		ArrayList<Libro> resultado = librosService.obtenerTodos();
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(misLibros, resultado);
		  
	}
	
	@Test
	public void obtenerLibroPorTitulo(){
		//arrange
		Libro libro = new Libro();
		libro.setTitulo("Java");
		
		
		Libro libroRetornado = new Libro();
		
		//act 
		when(librosDao.findOne(anyLong())).thenReturn(libro);
		libroRetornado = librosService.obtenerLibro(libro.getTitulo());
				
		//assert
		//Assert.assertNotNull(libroRetornado);	
		Assert.assertEquals(libro, libroRetornado);
	}
	
}











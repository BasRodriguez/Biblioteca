package com.nisum.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.nisum.dao.LibrosDao;
import com.nisum.model.Libro;


public class LibrosService {
	
	private final LibrosDao libroDao;

	//inyectar clase
	@Autowired 
	public LibrosService(LibrosDao libroDao){
		this.libroDao = libroDao;
	}

	//@Autowired
	//private LibrosDao libroDao;
		
	public Libro crearLibro(Libro libro) {
		// TODO Auto-generated method stub
		return libroDao.save(libro);
		
	}

	public Libro obtenerLibro(long id) {
		// TODO Auto-generated method stub
		Libro libro = new Libro();
		
		//libro.setId(id);
		libro = libroDao.findOne(id);
		return libro;
		
	}

	public ArrayList<Libro> obtenerTodos() {
		
		ArrayList<Libro> libros = new ArrayList<Libro>();
		Libro libro = new Libro();
		
		String titulo=libro.getTitulo();
		String autor=libro.getAutor();
		
		libros= (ArrayList<Libro>) libroDao.findLibros(titulo, autor);
		return libros;
	}
	
	public Libro obtenerLibro(String titulo) {
		// TODO Auto-generated method stub
		Libro libro = new Libro();
		
		libro = libroDao.findOne(libro.getId());
		return libro;
		
	}

}

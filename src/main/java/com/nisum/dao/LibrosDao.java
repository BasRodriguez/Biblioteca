package com.nisum.dao;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.nisum.model.Libro;

public interface LibrosDao extends CrudRepository<Libro, Long>{

	public Libro findByTitulo(String titulo);
	public ArrayList<Libro> findLibros(String titulo, String autor);

	
}

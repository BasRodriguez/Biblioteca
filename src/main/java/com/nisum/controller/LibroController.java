package com.nisum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import com.nisum.model.*;
import com.nisum.service.*;




@RequestMapping("/libro")
@RestController
public class LibroController {

	@Autowired
	private LibrosService libroService;
	
	@RequestMapping (value = "/crearLibro", method = POST)
	@ResponseBody
	public ResponseEntity<Libro>CrearLibro(@RequestBody Libro libro){
		libroService.crearLibro(libro);
		return new ResponseEntity<Libro>(libro, HttpStatus.CREATED);
	}
	
	@RequestMapping (value = "/obtenerLibro/{id}", method = GET)
	@ResponseBody
	public ResponseEntity<Libro>obtenerLibro(@PathVariable("id") long id){
		
		return new ResponseEntity<Libro>(libroService.obtenerLibro(id), HttpStatus.OK);
		
	}
	
	@RequestMapping (value = "/obtenerTodos", method = GET)
	@ResponseBody
	public ResponseEntity<List<Libro>> ObtenerTodos(){
		ResponseEntity<List<Libro>> response;
		
		List<Libro> list=libroService.obtenerTodos();
		response = new ResponseEntity<List<Libro>>(list, OK);
		return response;
	}
	
	@RequestMapping (value = "/obtenerLibroPorTitulo/{titulo}", method = GET)
	@ResponseBody
	public ResponseEntity<Libro>obtenerLibro(@PathVariable("titulo") String titulo){
		
		return new ResponseEntity<Libro>(libroService.obtenerLibro(titulo), HttpStatus.OK);
		
	}
}
